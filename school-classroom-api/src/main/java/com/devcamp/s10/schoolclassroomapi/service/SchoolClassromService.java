package com.devcamp.s10.schoolclassroomapi.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.s10.schoolclassroomapi.models.Classroom;
import com.devcamp.s10.schoolclassroomapi.models.School;

@Service
public class SchoolClassromService {
    Classroom classrom1 = new Classroom(101, "12C1", 35);
    Classroom classrom2 = new Classroom(102, "12C2", 45);
    Classroom classrom3 = new Classroom(103, "12C3", 40);

    Classroom classrom4 = new Classroom(104, "12C4", 40);
    Classroom classrom5 = new Classroom(105, "12C5", 50);
    Classroom classrom6 = new Classroom(106, "12C6", 45);

    Classroom classrom7 = new Classroom(107, "12C7", 35);
    Classroom classrom8 = new Classroom(108, "12C8", 45);
    Classroom classrom9 = new Classroom(109, "12C9", 55);


    public ArrayList<Classroom> getListClassrom1(){
       ArrayList <Classroom> listClassrom1 = new ArrayList<>();

       listClassrom1.add(classrom1);
       listClassrom1.add(classrom2);
       listClassrom1.add(classrom3);

       return listClassrom1;
    }
    public ArrayList<Classroom> getListClassrom2(){
        ArrayList <Classroom> listClassrom2 = new ArrayList<>();
 
        listClassrom2.add(classrom4);
        listClassrom2.add(classrom5);
        listClassrom2.add(classrom6);
 
        return listClassrom2;
     }
     public ArrayList<Classroom> getListClassrom3(){
        ArrayList <Classroom> listClassrom3 = new ArrayList<>();
 
        listClassrom3.add(classrom7);
        listClassrom3.add(classrom8);
        listClassrom3.add(classrom9);
 
        return listClassrom3;
     }

     public ArrayList <Classroom> getListClassromAll(){
        ArrayList <Classroom> listClassromAll = new ArrayList<>();
        listClassromAll.add(classrom1);
        listClassromAll.add(classrom2);
        listClassromAll.add(classrom3);
        listClassromAll.add(classrom4);
        listClassromAll.add(classrom5);
        listClassromAll.add(classrom6);
        listClassromAll.add(classrom7);
        listClassromAll.add(classrom8);
        listClassromAll.add(classrom9);
        return listClassromAll;
     }


     @Autowired
     public ArrayList<School> getListSchool(){
        ArrayList <School> listSchool = new ArrayList<>();
        School school1 = new School(011, "Hoang Hoa Tham", "Ninh Thuan", getListClassrom1());
        School school2 = new School(012, "Nguyen Trai", "Phan Rang", getListClassrom2());
        School school3 = new School(013, "Nguyen Van Linh", "Thuan Nam", getListClassrom3());

        listSchool.add(school1);
        listSchool.add(school2);
        listSchool.add(school3);

        return listSchool;
     }
}
