package com.devcamp.s10.schoolclassroomapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s10.schoolclassroomapi.models.Classroom;
import com.devcamp.s10.schoolclassroomapi.models.School;
import com.devcamp.s10.schoolclassroomapi.service.SchoolClassromService;

@RestController
@CrossOrigin
public class SchoolClassromController {
    @Autowired
    private SchoolClassromService schoolClassromService;


    // Tạo Get API lấy ra toàn bộ danh sách trường
    @GetMapping("/all-school")
    public ArrayList<School> getAllSchool(){
        ArrayList <School> listAllSchool = schoolClassromService.getListSchool();
        return listAllSchool;
    }


    // Tạo Get API truyền vào schoolId trả ra School tương ứng
    @GetMapping("/school-info")
    public School getSchoolInfo(@RequestParam(required = true, name="id") int schoolId) {
        ArrayList<School> allSchool = schoolClassromService.getListSchool();

        School findSchoolId = new School();
        for (School school : allSchool) {
            if(school.getId() == schoolId) {
                findSchoolId = school ;
            }
        }
        return findSchoolId;
    }


    // Tạo Get API trả ra danh sách tất cả các lớp 
    @GetMapping("/all-classroom")
    public ArrayList<Classroom> getAllClass(){
        ArrayList <Classroom> listClassAll = schoolClassromService.getListClassromAll();
        return listClassAll;
    }


    // Tạo Get API truyền vào noNumber trả ra danh sách lớp có số học sinh lớn hơn noNumber.
    @GetMapping("/classroom-find")
    public ArrayList<Classroom> getClassFromStudentQty(@RequestParam(required = true, name = "qty")int qty){
        ArrayList<Classroom> listAllClass = schoolClassromService.getListClassromAll();

        ArrayList<Classroom> findClass = new ArrayList<Classroom>();
        for(Classroom classroom : listAllClass){
            if(classroom.getNoStudent() >= qty)
            findClass.add(classroom);
        }
        return findClass;
    }

    // Tạo Get API truyền vào noNumber trả ra danh sách trường có số học sinh lớn hơn noNumber.
    @GetMapping("/school-find")
    public ArrayList<School> getSchoolFromStudentQty(@RequestParam(required = true, name = "qty") int qty){
        ArrayList <School> listAllSchool = schoolClassromService.getListSchool();

        ArrayList <School> findSchool = new ArrayList<School>();
        for (School school : listAllSchool){
            int sum = 0;
            sum = school.getTotalStudent();
            if(sum >= qty)
            findSchool.add(school);
        }
        return findSchool;
    }

}
